# Installation Instructions

In order to participate and perform the programming exercises of the course,
you need to have the following installed.

## Java JDK
* Java JDK, version 17 or later

## Build Tool
* Gradle
* Maven

## IDE
A decent IDE, such as any of

* JetBrains IntelliJ IDEA (*Community*)
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/download

## GIT Client
* https://git-scm.com/downloads

## SDKMAN
With _GIT for Windows_ comes a terminal application _GIT BASH_.
Based on `sdkman` you can install all Java tools.

* https://sdkman.io/

### Installation of sdkman
Open a GIT BASH window and first install ZIP.
Download `zip/unzip` and unpack its `bin` folder to `/usr/bin` within GIT BASH

* https://sourceforge.net/projects/gnuwin32/files/zip/3.0/zip-3.0-bin.zip/download
* https://sourceforge.net/projects/gnuwin32/files/unzip/5.51-1/unzip-5.51-1-bin.zip/download

The standard directory on Windows is

    C:\Program Files\Git\usr\bin

You can read more about various installation options here
* https://sdkman.io/install
* https://medium.com/@gayanper/sdkman-on-windows-661976238042
* https://stackoverflow.com/questions/38782928/how-to-add-man-and-zip-to-git-bash-installation-on-windows

Then, go ahead and install sdkman itself

    curl -s "https://get.sdkman.io" | bash

Finally, you can install

#### Java JDK

    sdk install java 20-open

#### Gradle

    sdk install gradle

#### Maven

    sdk install maven
