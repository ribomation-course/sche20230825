package ribomation;

import java.math.BigInteger;

public class Factorial {

    public long compute(long n) {
        if (n <= 0) return 0;
        if (n == 1) return 1;
        return n * compute(n - 1);
    }

    public BigInteger computeBig(long n) {
        if (n <= 0) return BigInteger.ZERO;
        if (n == 1) return BigInteger.ONE;

        var result = BigInteger.ONE;
        for (var k = 1; k <= n; ++k) {
            result = BigInteger.valueOf(k).multiply(result);
        }
        return result;
    }

}
