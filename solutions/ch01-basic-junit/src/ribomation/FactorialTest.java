package ribomation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.math.BigInteger;
import static org.junit.jupiter.api.Assertions.*;

class FactorialTest {
    Factorial target;

    @BeforeEach
    void setUp() {
        target = new Factorial();
    }

    @Test
    @DisplayName("base cases")
    void test1() {
        assertEquals(0, target.compute(0));
        assertEquals(1, target.compute(1));
    }

    @Test
    @DisplayName("known cases")
    void test2() {
        assertEquals(120, target.compute(5));
        assertEquals(3628800, target.compute(10));
    }

    @Test
    @DisplayName("known cases (big-int)")
    void test3() {
        assertEquals(BigInteger.valueOf(120), target.computeBig(5));
        assertEquals(BigInteger.valueOf(3628800), target.computeBig(10));
    }

    @Test
    @DisplayName("20! --> 2432902008176640000")
    void test4() {
        assertEquals(new BigInteger("2432902008176640000"), target.computeBig(20));
    }

    @Test
    @DisplayName("100! --> .....")
    void test5() {
        var result = "93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000";
        assertEquals(new BigInteger(result), target.computeBig(100));
    }

}
