package ribomation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringUtils {

    public String rightPad(String txt, int width, char fill) {
        if (txt == null) {
            return null;
        }
        if (txt.length() > width) {
            throw new IllegalArgumentException();
        }

        int padWidth = width - txt.length();
        StringBuilder result = new StringBuilder();
        while (padWidth-- > 0) result.append(fill);
        return result.append(txt).toString();
    }

    public List<String> splitCamelCase(String s) {
        var lst = new ArrayList<String>();
        StringBuilder word = new StringBuilder();
        for (var k = 0; k < s.length(); ++k) {
            var ch = s.charAt(k);
            if (Character.isUpperCase(ch)) {
                if (!word.isEmpty()) {
                    lst.add(word.toString());
                    word = new StringBuilder();
                }
            }
            word.append(Character.toLowerCase(ch));
        }
        if (!word.isEmpty()) lst.add(word.toString());

        return lst;

    }

}