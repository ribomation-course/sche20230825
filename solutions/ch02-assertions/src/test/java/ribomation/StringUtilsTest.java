package ribomation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StringUtilsTest {
    StringUtils target;

    @BeforeEach
    void setUp() {
        target = new StringUtils();
    }

    @Test
    @DisplayName("right pad")
    void rightPad() {
        assertNull(target.rightPad(null, 10, '*'));
        assertThrows(IllegalArgumentException.class, () -> {
            target.rightPad("hello", 2, '*');
        });
        assertEquals("*****hello", target.rightPad("hello", 10, '*'));
    }

    @Test
    @DisplayName("split camel case")
    void splitCamelCase() {
        assertIterableEquals(
                List.of("unit", "testing", "is", "fun"),
                target.splitCamelCase("unitTestingIsFun"));
    }
}
