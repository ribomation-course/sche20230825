package ribomation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CaseConverter {
    public String camel2train(String s) {
        if (s == null || s.trim().isEmpty()) {
            return s;
        }
        return split(s).stream()
                .map(this::capitalize)
                .collect(Collectors.joining("-"));
    }

    String capitalize(String s) {
        if (s == null || s.trim().isEmpty()) {
            return s;
        }
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
    }

    List<String> split(String s) {
        var lst = new ArrayList<String>();
        StringBuilder word = new StringBuilder();
        for (var k = 0; k < s.length(); ++k) {
            var ch = s.charAt(k);
            if (Character.isUpperCase(ch)) {
                if (!word.isEmpty()) {
                    lst.add(word.toString());
                    word = new StringBuilder();
                }
            }
            word.append(ch);
        }
        lst.add(word.toString());
        return lst;
    }
}