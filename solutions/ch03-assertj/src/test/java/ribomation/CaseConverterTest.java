package ribomation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.*;

class CaseConverterTest {
    CaseConverter target;

    @BeforeEach
    void setUp() {
        target = new CaseConverter();
    }

    @Test
    @DisplayName("base cases")
    void test1() {
        assertThat(target.camel2train(null)).isNull();
        assertThat(target.camel2train("")).isEmpty();
    }

    @Test
    @DisplayName("word of lower case letters should return it capitalized")
    void test2() {
        assertThat(target.camel2train("abc123")).isEqualTo("Abc123");
    }

    @Test
    @DisplayName("hippHapp --> Hipp-Happ")
    void test3() {
        assertThat(target.camel2train("hippHapp"))
                .isEqualTo("Hipp-Happ");
    }

    @Test
    @DisplayName("hippHappHopp --> Hipp-Happ-Hopp")
    void test4() {
        assertThat(target.camel2train("hippHappHopp"))
                .startsWith("Hipp")
                .endsWith("Hopp")
                .contains("-")
                .doesNotContainAnyWhitespaces();
    }


}