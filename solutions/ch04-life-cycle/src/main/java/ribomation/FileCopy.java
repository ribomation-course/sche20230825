package ribomation;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileCopy {
    void copy(InputStream in, Path out) throws IOException {
        Files.copy(in, out);
    }
}