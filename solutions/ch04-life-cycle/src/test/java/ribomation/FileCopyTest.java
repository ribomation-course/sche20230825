package ribomation;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.assertj.core.api.Assertions.*;

class FileCopyTest {
    FileCopy target;
    InputStream indata;
    Path tmpDir;

    @BeforeEach
    void setUp() throws IOException {
        indata = getClass().getResourceAsStream("/data.txt");
        assertThat(indata).isNotNull();
        tmpDir = Files.createTempDirectory("file-copy-test");
        target = new FileCopy();
    }

    @AfterEach
    void tearDown() throws IOException {
        try (var lst = Files.list(tmpDir)) {
            lst.forEach(f -> {
                try {
                    Files.delete(f);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }
        Files.delete(tmpDir);
    }

    @Test
    void copy() throws IOException {
        var outfile = tmpDir.resolve("outfile.txt");
        target.copy(indata, outfile);
        assertThat(Files.size(outfile)).isGreaterThan(0);
        assertThat(Files.readString(outfile)).contains("kingdom");
        assertThat(Files.readAllLines(outfile).size()).isEqualTo(2);
    }
}
