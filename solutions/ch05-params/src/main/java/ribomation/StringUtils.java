package ribomation;

public class StringUtils {
    public boolean isEmail(String txt) {
        if (txt == null || txt.trim().isEmpty()) return false;

        String[] parts = txt.split("@");
        if (parts.length != 2) return false;

        String name   = parts[0];
        String domain = parts[1];

        parts = domain.split("\\.");
        if (parts.length < 2) return false;
        String tld = parts[parts.length - 1];

        if (!Character.isLetter(name.charAt(0))) return false;
        if (name.charAt(name.length() - 1) == '.') return false;
        if (!name.matches("^[a-zA-Z0-9.-]+$")) return false;

        if (!Character.isLetter(domain.charAt(0))) return false;
        if (domain.charAt(domain.length() - 1) == '.') return false;
        if (!(tld.length() == 2 || tld.length() == 3)) return false;

        return true;
    }
}
