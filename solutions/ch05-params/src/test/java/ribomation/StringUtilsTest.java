package ribomation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import static org.assertj.core.api.Assertions.assertThat;

class StringUtilsTest {
    StringUtils target;

    @BeforeEach
    void init() {
        target = new StringUtils();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/emails-ok.txt")
    void correct_emails(String email) {
        assertThat(target.isEmail(email)).isTrue();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/emails-faulty.txt")
    void faulty_emails(String email) {
        assertThat(target.isEmail(email)).isFalse();
    }

}
