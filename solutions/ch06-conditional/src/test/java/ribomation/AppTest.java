package ribomation;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfSystemProperty;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @BeforeAll
    static void setup() {
        System.setProperty("ribomation.mode", "DEV");
    }

    @Test
    @DisplayName("should run in DEV mode")
    @EnabledIfSystemProperty(named = "ribomation.mode", matches = "^DEV$")
    void test1() {
        assertTrue(true);
    }

    @Test
    @DisplayName("should run in QA mode")
    @EnabledIfSystemProperty(named = "ribomation.mode", matches = "^QA$")
    void test2() {
        assertTrue(true);
    }
}
