void copyFile(InputStream in, Path out) {
    if (in == null) {
        throw new IllegalArgumentException("input-stream was null");
    }
    if (out == null) {
        throw new IllegalArgumentException("output-stream was null");
    }

    try {
        Files.copy(in, out, StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException e) {
        throw new IllegalArgumentException(e.getMessage());
    }
}
