package ribomation.unit_tests;

import java.util.Map;

public interface Database {
    void insert(String sql);

    int count(String tblName);

    Map<String, String> firstRow(String sql);

   // void remove(String sql);
}

