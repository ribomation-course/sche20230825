package ribomation.unit_tests;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("DB mock sample")
public class DatabaseTest {
    @Mock
    Database db;
    PersonDAO target;

    @BeforeEach
    public void init() {
        target = new PersonDAO(db);
    }

    @Test
    @DisplayName("There should be 3 persons, when asking for count()")
    public void there_should_be_3_persons() {
        when(db.count("persons")).thenReturn(3);

        assertThat(target.numPersons()).isEqualTo(3);

        verify(db, times(1)).count("persons");
        verify(db, never()).firstRow(anyString());
    }

    @Test
    @DisplayName("Should be possible to find a person")
    public void should_find_person_in_db() {
        Map<String, String> anna = new HashMap<String, String>();
        anna.put("name", "Anna Conda");
        anna.put("age", "42");
        when(db.firstRow("SELECT * FROM persons WHERE name = 'anna'")).thenReturn(anna);

        Person p = target.find("anna");
        assertThat(p).isNotNull();
        assertThat(p.getName()).endsWith("onda");
        assertThat(p.getAge()).isEqualTo(42);

        verify(db, times(1)).firstRow(anyString());
        verify(db, times(0)).count("persons");
    }

    @Test
    @DisplayName("Looking for a non-existing person should throw an exception")
    public void should_return_null_if_not_found() {
        when(db.firstRow("SELECT * FROM persons WHERE name = 'boris'")).thenReturn(null);

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> {
                    target.find("boris");
                })
                .withMessage("No such person name: boris")
        ;

        verify(db, times(1)).firstRow(anyString());
    }


    @Test
    @DisplayName("Inserting an extra person, should increase count by one")
    public void insert_should_increment_person_count() {
        when(db.count("persons"))
                .thenReturn(3)
                .thenReturn(4);

        assertThat(target.numPersons()).isEqualTo(3);

        Person nisse  = new Person("Nisse", 45);
        Person result = target.insert(nisse);

        assertThat(result).isNotNull();
        assertThat(target.numPersons()).isEqualTo(4);

        verify(db, times(2)).count("persons");
        verify(db, times(1)).insert("INSERT INTO persons (name,age) VALUES ('Nisse',45)");
    }

    @Test
    @DisplayName("Removing a person, should decrese the count by one")
    void remove() {
        final String        NAME       = "Nisse";
        final int           AGE        = 45;
        Map<String, String> personData = new HashMap<String, String>();
        personData.put("name", NAME);
        personData.put("age", ""+AGE);

        when(db.firstRow("SELECT * FROM persons WHERE name = 'Nisse'"))
                .thenReturn(personData)
                .thenReturn(null);
        when(db.count("persons"))
                .thenReturn(3)
                .thenReturn(2);

        int count = target.numPersons();
        assertThat(count).isEqualTo(3);
        Person p = target.find(NAME);
        assertThat(p).isNotNull();
        assertThat(p.getName()).isEqualTo(NAME);

        target.remove(new Person(NAME, AGE));
        // count should be one less
        // shouldn't find Nisse
        // the sql delete statement should have been run once

    }

}

