List<String> splitCamelCase(String camelCaseText) {
    List<String> words = new ArrayList<>();

    String word = "";
    for (int k = 0; k < camelCaseText.length(); ++k) {
        char ch = camelCaseText.charAt(k);
        if (Character.isUpperCase(ch)) {
            words.add(word);
            word = "" + Character.toLowerCase(ch);
        } else {
            word += ch;
        }
    }
    if (!word.isEmpty()) {
        words.add(word);
    }

    return words;
}
